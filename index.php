<!DOCTYPE html>
<html>
  <head>
    <title>Geocoding service</title>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 30%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      #floating-panel {
        position: absolute;
        top: 10px;
        left: 25%;
        z-index: 5;
        background-color: #fff;
        padding: 5px;
        border: 1px solid #999;
        text-align: center;
        font-family: 'Roboto','sans-serif';
        line-height: 30px;
        padding-left: 10px;
      }
      
      #location{
      	display: block;
      	width:fit-content;
      	margin: auto;
      	margin-top: 10px;
      	}
      	
      .half{
      	margin-top: 50px;
      	width: 50%;
      	min-height: 200px;
      	float: left;
      	box-sizing: border-box;
      	border: 1px solid black;
      	}
      	
      .container{
      	display:block;
      	clear:both;
      	overflow:auto;
      	width: 100%;
      	height:200px;
      	background: lightgrey;
      	margin-top: 10px;
      	}
      
      .header{
      	width: 100%;
      	background: grey;
      	text-align: center;
      	}
    </style>
  </head>
  <body>
    <div id="floating-panel">
      <input id="address" type="textbox" value="Sydney, NSW">
      <input id="submit" type="button" value="Geocode">
    </div>
    <div id="map"></div>
    <form id="location"> <input id="lat" name="lat" readonly /> <input id="lng" name="lng" readonly /></form>
    <div id="box">
    	<div id="given" class="half">
    		
    	</div>
    	<div id="mine" class="half">
    		
    	</div>
    </div>
    <script>
    	var latitude;
    	var longitude;
    	
      function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 8,
          center: {lat: -34.397, lng: 150.644}
        });
        var geocoder = new google.maps.Geocoder();

        document.getElementById('submit').addEventListener('click', function() {
          geocodeAddress(geocoder, map);
        });
      }

      function geocodeAddress(geocoder, resultsMap) {
        var address = document.getElementById('address').value;
        geocoder.geocode({'address': address}, function(results, status) {
          if (status === 'OK') {
            resultsMap.setCenter(results[0].geometry.location);
            var marker = new google.maps.Marker({
              map: resultsMap,
              position: results[0].geometry.location
             
            });
            document.getElementById('lat').value = results[0].geometry.location.lat();
       			document.getElementById('lng').value = results[0].geometry.location.lng();
       			$('#lat').change();
            
          } else {
            alert('Geocode was not successful for the following reason: ' + status);
          }
        });
      }
    </script>
    <script>
    	var build = function(data){
    		
    		for(day in data.mine){
    			
    			givenid='given'+day;
    			var element = document.getElementById(givenid);
    			if(element != null){
    				element.parentNode.removeChild(element);
    				}
    			var container = document.createElement("div");
    			var header = document.createElement("h2");
    			header.className="header";
    			header.innerHTML=day;
    			container.id= givenid;
    			container.className="container";
    			document.getElementById("given").appendChild(container);
    			document.getElementById(givenid).appendChild(header);
    			var i = 0;
    			for(key in data.mine[day]['hourly']){
    				i=i+1;
    				var img = document.createElement("img");
    				var label = document.createElement("label");
    				if(data.mine[day]['hourly'][key]['night']==1){
    					var night = 1;
    					label.innerHTML=key+"&#9679";
    					}else{
    						
    						var night = 0;
    						label.innerHTML=key+"&#9678";
    						};
    				
    				label.setAttribute("for",i+day);
    				
    				img.src = 'https://api.met.no/weatherapi/weathericon/1.1?symbol='+data.mine[day]['hourly'][key]['id'][0]+'&is_night='+night+'&content_type=image%2Fpng';
    				img.id=i+day;
    				document.getElementById(givenid).appendChild(img);
    				document.getElementById(givenid).appendChild(label);
    				}
    			
    		}
    		
    		for(day in data.mine){
    			mineid='mine'+day;
    			var element = document.getElementById(mineid);
    			if(element != null){
    				element.parentNode.removeChild(element);
    				}
    			var container = document.createElement("div");
    			var header = document.createElement("h2");
    			header.className="header";
    			header.innerHTML=day;
    			container.id= mineid;
    			container.className="container";
    			document.getElementById("mine").appendChild(container);
    			document.getElementById(mineid).appendChild(header);
                         var iconnameday = document.createElement("p");
                        iconnameday.innerHTML = data.mine[day]['daysymb']['name']; 
                        document.getElementById(mineid).appendChild(iconnameday);
                        
                        var iconnamenight = document.createElement("p");
                        iconnamenight.innerHTML = data.mine[day]['nightsymb']['name']; 
                        document.getElementById(mineid).appendChild(iconnamenight);
                       
    				
    			
    		}
    
    }
    	$('#lat').change(function(){
    			var location = $('#location').serialize();
    			console.log('send');
    		$.ajax({
    			url:"ajax.php",
    			method: "POST",
    			data: location,
    			dataType: "json",
    			success: function(data){
    				build(data);
    				}
    			});
    		})
    	
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDZIAWVGhdwNwK_KMENuUUmpX9Qb7KqirA&callback=initMap">
    </script>
  </body>
</html>