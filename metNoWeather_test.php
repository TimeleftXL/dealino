<?php

class metNoWeather {

    private function _getUrlContent($query){

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $query);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt ($ch, CURLOPT_HEADER, 0);
        curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_TIMEOUT, 50);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 6);
        curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt ($ch, CURLOPT_HTTPHEADER,array('User-agent: Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.16) Gecko/20080702 Firefox/2.0.0.16', 'Accept-language: en-us,en;q=0.7,bn;q=0.3', 'Accept-charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7'));
        $content = curl_exec($ch);
        $httpStatus = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $lastUrl = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);


        return array(
            'content' => $content,
            'httpStatus' => $httpStatus,
            'lastUrl' => $lastUrl
        );

    }

    public function getLocationForeCastNow($lat, $lng){

        $query = sprintf('http://api.met.no/weatherapi/locationforecast/1.9/?lat=%s;lon=%s',
            $lat,
            $lng
        );

        $result = $this->_getUrlContent($query);

        $content = $result['content'];

        $xml = simplexml_load_string($content);



        $now = array(
            'temperature' => 0,
            'windDirection' => '',
            'windDirectionDeg' => 0,
            'windSpeed' => 0,
            'pressure' => 0,
            'symbol' => 0
        );

        $sun = true;

        $sun_info = date_sun_info(time(), $lat, $lng);

        if($sun_info['sunset']<time())$sun = false;


        if(
            isset($xml->product) &&
            isset($xml->product->time[0]) &&
            isset($xml->product->time[0]->location)
        ){

            $location = $xml->product->time[0]->location;


            if(isset($location->temperature)){
                $now['temperature'] = (float)$location->temperature['value'];
            }
            if(isset($location->windDirection)){
                $now['windDirection'] = (string)$location->windDirection['name'];
            }
            if(isset($location->windDirection)){
                $now['windDirectionDeg'] = (float)$location->windDirection['deg'];
            }
            if(isset($location->windSpeed)){
                $now['windSpeed'] = (float)$location->windSpeed['mps']*3.6;
            }
            if(isset($location->pressure)){
                $now['pressure'] = (float)$location->pressure['value'];
            }
            if(isset($location->humidity)){
                $now['humidity'] = (float)$location->humidity['value'];
            }
            if(isset($location->cloudiness)){
                $now['cloudiness'] = (float)$location->cloudiness['percent'];
            }
        }

        if(
            isset($xml->product) &&
            isset($xml->product->time[1]) &&
            isset($xml->product->time[1]->location)
        ){

            $location = $xml->product->time[1]->location;

            if(isset($location->symbol)){
                $now['symbol'] = $this->translateIcons((int)$location->symbol['number'], $sun);
                $now['symbol_original'] = (int)$location->symbol['number'];
            }
            if(isset($location->precipitation)){
                $now['precipitation'] = (float)$location->precipitation['value'];
            }
        }

        $todayTemperatures = [];
        $date = date('Y-m-d');

        $todayCloudiness = [];

        foreach($xml->product->time as $time){
            $to = $time['to'];
            if(substr($time['to'], 0, 10) === $date) {
                if(
                    isset($time->location) &&
                    isset($time->location->minTemperature) &&
                    isset($time->location->maxTemperature)
                ){
                    $todayTemperatures[] = (float)$time->location->minTemperature['value'];
                    $todayTemperatures[] = (float)$time->location->maxTemperature['value'];
                }
                /*

                $hour = substr($time['to'], 11, 2);

                if($hour && isset($time->location->cloudiness)){
                    $temp = array(
                        'time' => $hour,
                        'cl' => (float)$time->location->cloudiness['percent']
                    );
                    $todayCloudiness[] = $temp;
                }*/

                //print_r($time);

            } else break;
        }

        $tomorrow = date('Y-m-d', strtotime('+1 day'));

        $hourlyKeyed = [];
        $dailyKeyed = [];

        foreach($xml->product->time as $time){
            $to = $time['to'];
            $from = $time['from'];
            if(substr($time['to'], 0, 10) === $date || substr($time['to'], 0, 10) === $tomorrow || true) {

                $hour = substr($time['to'], 11, 2);

                if($hour && isset($time->location->cloudiness) && (($hour>17 && substr($time['to'], 0, 10) === $date) || ($hour<8 && substr($time['to'], 0, 10) === $tomorrow))){
                    $temp = array(
                        'time' => $hour,
                        'cl' => (float)$time->location->cloudiness['percent'],
                        'date' => (string)substr($time['to'], 0, 10)
                    );
                    $todayCloudiness[] = $temp;
                }

                /*3h Preview Dashboard*/



                $toHour = date('H', strtotime($to));
                $fromHour = date('H', strtotime($from));


                $key = date('Y-m-d H:i:s', strtotime($to));

                $fromDate = date('Y-m-d', strtotime($from));
                $toDate = date('Y-m-d', strtotime($to));

                $fromDateH = date('Y-m-d H:i:s', strtotime($from));
                $toDateH = date('Y-m-d H:i:s', strtotime($to));

                //echo $from.' '.$to.' '.$fromDate.' '.$toDate."\n";

                if($fromDate === $toDate){
                    $keyDaily = $toDate;
                } else {
                    $keyDaily = $fromDate;
                }

                $sun_info = date_sun_info(strtotime($to), $lat, $lng);
                $sun_info_next = date_sun_info((strtotime($to)+(24*60*60)), $lat, $lng);

                $sun = true;

                if(($sun_info['sunset']<strtotime($to) && $sun_info['sunrise']<strtotime($to)) || ($sun_info['sunset']>strtotime($to) && $sun_info['sunrise']>strtotime($to)))$sun = false;

                //print_r($to);
                //print_r(strtotime($to));
                //print_r($sun_info);

                if(
                    isset($time->location)
                ){
                    $location = $time->location;

                    if(abs($fromHour-$toHour)<2 || ($fromHour=='23' && $toHour=='00')){

                        if(isset($location->temperature)){
                            $hourlyKeyed[$key]['temperature'] = (float)$location->temperature['value'];
                            $dailyKeyed[$keyDaily]['temperature'][] = (float)$location->temperature['value'];
                        }
                        if(isset($location->windDirection)){
                            $hourlyKeyed[$key]['windDirection'] = (string)$location->windDirection['name'];
                        }
                        if(isset($location->windDirection)){
                            $hourlyKeyed[$key]['windDirectionDeg'] = (float)$location->windDirection['deg'];
                        }
                        if(isset($location->windSpeed)){
                            $hourlyKeyed[$key]['windSpeed'] = (float)$location->windSpeed['mps']*3.6;
                            $dailyKeyed[$keyDaily]['windSpeed'][] = (float)$location->windSpeed['mps']*3.6;
                        }
                        if(isset($location->pressure)){
                            //$hourlyKeyed[$key]['pressure'] = (float)$location->pressure['value'];
                        }
                        if(isset($location->humidity)){
                            //$hourlyKeyed[$key]['humidity'] = (float)$location->humidity['value'];
                        }
                        if(isset($location->cloudiness)){
                            $hourlyKeyed[$key]['cloudiness'] = (float)$location->cloudiness['percent'];
                            $dailyKeyed[$keyDaily]['cloudiness'][] = (float)$location->cloudiness['percent'];
                        }
                        if(isset($location->symbol)){
                            $hourlyKeyed[$key]['symbol'] = $this->translateIcons((int)$location->symbol['number'], $sun);
                            //$hourlyKeyed[$key]['symbol_original'] = (int)$location->symbol['number'];
                            $dailyKeyed[$keyDaily]['symbol_original'][] = (int)$location->symbol['number'];
                            //echo $from.' - '.$to.' - '.(int)$location->symbol['number']."\n";
                        }
                        if(isset($location->precipitation)){
                            $hourlyKeyed[$key]['precipitation'] = (float)$location->precipitation['value'];
                            $dailyKeyed[$keyDaily]['precipitation'][] = (float)$location->precipitation['value'];
                        }

                    }

                    if(($fromHour=='02' && $toHour=='08') || ($fromHour=='08' && $toHour=='14') || ($fromHour=='14' && $toHour=='20') || ($fromHour=='20' && $toHour=='02')){
                        if(isset($location->symbol)){
                            $dailyKeyed[$keyDaily]['symbol_original_6'][] = (int)$location->symbol['number'];
                        } 
                    }

                    if(isset($location->minTemperature)){
                        $dailyKeyed[$keyDaily]['minTemperature'][] = (float)$location->minTemperature['value'];
                    }
                    if(isset($location->maxTemperature)){
                        $dailyKeyed[$keyDaily]['maxTemperature'][] = (float)$location->maxTemperature['value'];
                    }
                    if(isset($location->symbol)){
                        $dailyKeyed[$keyDaily]['symbol_original_2'][] = (int)$location->symbol['number'];
                    }
                    if(isset($location->precipitation)){
                        $dailyKeyed[$keyDaily]['precipitation_2'][] = (float)$location->precipitation['value'];
                    }
                    if(isset($location->windDirection)){
                        $dailyKeyed[$keyDaily]['windDirectionDeg'][] = (float)$location->windDirection['deg'];
                    }

                }

            } //else break;
        }


        $hourly = [];

        $i = 0;
        foreach ($hourlyKeyed as $key => $value) {
            $i++;
            $detail = $value;
            $detail['date'] = strtotime($key);
            $hourly[] = $detail;
            if($i>24)break;

        }

        $now['hourly'] = $hourly;


        $daily = [];

        $i = 0;

        //print_r($dailyKeyed);

        foreach ($dailyKeyed as $key => $value) {
            $i++;
            $detail = [];
            $detail['minTemperature'] = min($value['minTemperature']);
            $detail['maxTemperature'] = max($value['maxTemperature']);
            $detail['maxWindSpeed'] = max($value['windSpeed']);
            //$detail['pressure'] = round(array_sum($value['pressure'])/count($value['pressure']));
            //$detail['humidity'] = round(array_sum($value['humidity'])/count($value['humidity']));
            $detail['windDirectionDeg'] = round(array_sum($value['windDirectionDeg'])/count($value['windDirectionDeg']));

            if(is_array($value['precipitation']))
                $detail['precipitation'] = array_sum($value['precipitation']);
            elseif (is_array($value['precipitation_2'])) {
                $detail['precipitation'] = array_sum($value['precipitation_2']);
            }
            $detail['cloudiness'] = round(array_sum($value['cloudiness'])/count($value['cloudiness']));


            //print_r($value['symbol_original']);
            //if(count($value['symbol_original']) !== 24)print_r($value['symbol_original_6']);
            //print_r($key);

            $todayKey = date('Y-m-d');
            if(
                $key!==$todayKey
            ){
                if(count($value['symbol_original']) === 24){
                    $first = array_chunk($value['symbol_original'], 12)[0];
                    $second = array_chunk($value['symbol_original'], 12)[0];
                    $firstValues = array_count_values($first);
                    $max = max($firstValues);
                    foreach($firstValues as $k=>$v){
                        if($v==$max){
                            //$detail['symbol_original_d'] = $k;
                            $detail['symbol_d'] = $this->translateIcons($k, true);
                            break;
                        }
                    }
                    $secondValues = array_count_values($second);
                    $max = max($secondValues);
                    foreach($secondValues as $k=>$v){
                        if($v==$max){
                            //$detail['symbol_original_n'] = $k;
                            $detail['symbol_n'] = $this->translateIcons($k, false);
                            break;
                        }
                    }                 
                }
                if(count($value['symbol_original']) !== 24 && count($value['symbol_original_6']) === 4){
                    //$detail['symbol_original_d'] = $value['symbol_original_6'][1];
                    $detail['symbol_d'] = $this->translateIcons($value['symbol_original_6'][1], true);
                    //$detail['symbol_original_n'] = $value['symbol_original_6'][3];
                    $detail['symbol_n'] = $this->translateIcons($value['symbol_original_6'][3], false);                    
                }
            }

            if(is_array($value['symbol_original'])){
                $values = array_count_values($value['symbol_original']);
                //$detail['symbol_original_sort'] = $values;
                $max = max($values);
                foreach($values as $k=>$v){
                    if($v==$max){
                        $detail['symbol_original'] = $k;
                        $detail['symbol'] = $this->translateIcons($k, true);
                        break;
                    }
                }

            }elseif (is_array($value['symbol_original_2'])) {
                $values = array_count_values($value['symbol_original_2']);
                //print_r($value['symbol_original_2']);
                //print_r($key);
                //print_r($values);
                //$detail['symbol_original_sort'] = $values;
                $max = max($values);
                foreach($values as $k=>$v){
                    if($v==$max){
                        $detail['symbol_original'] = $k;
                        $detail['symbol'] = $this->translateIcons($k, true);
                        break;
                    }
                }
            }

            //$detail = $value;
            $detail['date'] = strtotime($key);
            $daily[] = $detail;

            //if($i>6)break;

        }

        //print_r($daily);

        $now['daily'] = $daily;


        $now['maxTemperature'] = max($todayTemperatures);
        $now['minTemperature'] = min($todayTemperatures);

        $now['cloudinessHours'] = $todayCloudiness;


        return $now;
    }

    public function getLocationForeCast($lat, $lng){

        $query = sprintf('http://api.met.no/weatherapi/locationforecast/1.9/?lat=%s;lon=%s',
            $lat,
            $lng
        );

        $result = $this->_getUrlContent($query);

        $content = $result['content'];

        $xml = simplexml_load_string($content);

        //print_r($xml);

        $out = array();


        $now = array(
            'temperature' => 0,
            'windDirection' => '',
            'windDirectionDeg' => 0,
            'windSpeed' => 0,
            'pressure' => 0,
            'symbol' => 0
        );


        $sun = true;

        $sun_info = date_sun_info(time(), $lat, $lng);

        if($sun_info['sunset']<time())$sun = false;
        

        if(
            isset($xml->product) &&
            isset($xml->product->time[0]) &&
            isset($xml->product->time[0]->location)
        ){

            $location = $xml->product->time[0]->location;


            if(isset($location->temperature)){
                $now['temperature'] = (float)$location->temperature['value'];
            }
            if(isset($location->windDirection)){
                $now['windDirection'] = (string)$location->windDirection['name'];
            }
            if(isset($location->windDirection)){
                $now['windDirectionDeg'] = (float)$location->windDirection['deg'];
            }
            if(isset($location->windSpeed)){
                $now['windSpeed'] = (float)$location->windSpeed['mps']*3.6;
            }
            if(isset($location->pressure)){
                $now['pressure'] = (float)$location->pressure['value'];
            }
            if(isset($location->humidity)){
                $now['humidity'] = (float)$location->humidity['value'];
            }
            if(isset($location->cloudiness)){
                $now['cloudiness'] = (float)$location->cloudiness['percent'];
            }
        }

        if(
            isset($xml->product) &&
            isset($xml->product->time[1]) &&
            isset($xml->product->time[1]->location)
        ){

            $location = $xml->product->time[1]->location;

            if(isset($location->symbol)){
                $now['symbol'] = $this->translateIcons((int)$location->symbol['number'], $sun);
                $now['symbol_original'] = (int)$location->symbol['number'];
            }
            if(isset($location->precipitation)){
                $now['precipitation'] = (float)$location->precipitation['value'];
            }
        }

        $todayTemperatures = [];
        $date = date('Y-m-d');

        foreach($xml->product->time as $time){
            $to = $time['to'];
            if(substr($time['to'], 0, 10) === $date) {
                if(
                    isset($time->location) &&
                    isset($time->location->minTemperature) &&
                    isset($time->location->maxTemperature)
                ){
                    $todayTemperatures[] = (float)$time->location->minTemperature['value'];
                    $todayTemperatures[] = (float)$time->location->maxTemperature['value'];
                }

            } else break;
        }

        $now['maxTemperature'] = max($todayTemperatures);
        $now['minTemperature'] = min($todayTemperatures);

        $out['today'] = $now;

        $hourlyKeyed = [];

        $dailyKeyed = [];



        foreach($xml->product->time as $time){

            $to = $time['to'];
            $from = $time['from'];

            $toHour = date('H', strtotime($to));
            $fromHour = date('H', strtotime($from));



                //print_r('from'.' '.$from.' ');

                //print_r('to'.' '.$to.' '.$toHour);

                //echo "\n";


                $key = date('Y-m-d H:i:s', strtotime($to));


                $fromDate = date('Y-m-d', strtotime($from));
                $toDate = date('Y-m-d', strtotime($to));

                $fromDateH = date('Y-m-d H:i:s', strtotime($from));
                $toDateH = date('Y-m-d H:i:s', strtotime($to));

                //echo $from.' '.$to.' '.$fromDate.' '.$toDate."\n";

                if($fromDate === $toDate){
                    $keyDaily = $toDate;
                } else {
                    $keyDaily = $fromDate;
                }

                $sun_info = date_sun_info(strtotime($to), $lat, $lng);
                $sun_info_next = date_sun_info((strtotime($to)+(24*60*60)), $lat, $lng);

                $sun = true;

                if(($sun_info['sunset']<strtotime($to) && $sun_info['sunrise']<strtotime($to)) || ($sun_info['sunset']>strtotime($to) && $sun_info['sunrise']>strtotime($to)))$sun = false;

                //print_r($to);
                //print_r(strtotime($to));
                //print_r($sun_info);

                if(
                    isset($time->location)
                ){
                    $location = $time->location;

                    if(abs($fromHour-$toHour)<2 || ($fromHour=='23' && $toHour=='00')){

                        if(isset($location->temperature)){
                            $hourlyKeyed[$key]['temperature'] = (float)$location->temperature['value'];
                            $dailyKeyed[$keyDaily]['temperature'][] = (float)$location->temperature['value'];
                        }
                        if(isset($location->windDirection)){
                            $hourlyKeyed[$key]['windDirection'] = (string)$location->windDirection['name'];
                        }
                        if(isset($location->windDirection)){
                            $hourlyKeyed[$key]['windDirectionDeg'] = (float)$location->windDirection['deg'];
                        }
                        if(isset($location->windSpeed)){
                            $hourlyKeyed[$key]['windSpeed'] = (float)$location->windSpeed['mps']*3.6;
                            $dailyKeyed[$keyDaily]['windSpeed'][] = (float)$location->windSpeed['mps']*3.6;
                        }
                        if(isset($location->pressure)){
                            $hourlyKeyed[$key]['pressure'] = (float)$location->pressure['value'];
                            $dailyKeyed[$keyDaily]['pressure'][] = (float)$location->pressure['value'];
                        }
                        if(isset($location->humidity)){
                            $hourlyKeyed[$key]['humidity'] = (float)$location->humidity['value'];
                            $dailyKeyed[$keyDaily]['humidity'][] = (float)$location->humidity['value'];
                        }
                        if(isset($location->cloudiness)){
                            $hourlyKeyed[$key]['cloudiness'] = (float)$location->cloudiness['percent'];
                            $dailyKeyed[$keyDaily]['cloudiness'][] = (float)$location->cloudiness['percent'];
                        }
                        if(isset($location->symbol)){
                            $hourlyKeyed[$key]['symbol'] = $this->translateIcons((int)$location->symbol['number'], $sun);
                            $hourlyKeyed[$key]['symbol_original'] = (int)$location->symbol['number'];
                            $dailyKeyed[$keyDaily]['symbol_original'][] = (int)$location->symbol['number'];
                        }
                        if(isset($location->precipitation)){
                            $hourlyKeyed[$key]['precipitation'] = (float)$location->precipitation['value'];
                            $dailyKeyed[$keyDaily]['precipitation'][] = (float)$location->precipitation['value'];
                        }

                    }

                    if(($fromHour=='02' && $toHour=='08') || ($fromHour=='08' && $toHour=='14') || ($fromHour=='14' && $toHour=='20') || ($fromHour=='20' && $toHour=='02')){
                        if(isset($location->symbol)){
                            $dailyKeyed[$keyDaily]['symbol_original_6'][] = (int)$location->symbol['number'];
                        } 
                    }
                    if(isset($location->minTemperature)){
                        $dailyKeyed[$keyDaily]['minTemperature'][] = (float)$location->minTemperature['value'];
                    }
                    if(isset($location->maxTemperature)){
                        $dailyKeyed[$keyDaily]['maxTemperature'][] = (float)$location->maxTemperature['value'];
                    }
                    if(isset($location->symbol)){
                        $dailyKeyed[$keyDaily]['symbol_original_2'][] = (int)$location->symbol['number'];
                    }
                    if(isset($location->precipitation)){
                        $dailyKeyed[$keyDaily]['precipitation_2'][] = (float)$location->precipitation['value'];
                    }
                    if(isset($location->windDirection)){
                        $dailyKeyed[$keyDaily]['windDirectionDeg'][] = (float)$location->windDirection['deg'];
                    }

                }


        }

        $hourly = [];

        $i = 0;
        foreach ($hourlyKeyed as $key => $value) {
            $i++;
            $detail = $value;
            $detail['date'] = strtotime($key);
            $hourly[] = $detail;
            if($i>24)break;

        }

        $out['hourly'] = $hourly;


        $daily = [];

        $i = 0;

        var_dump($dailyKeyed);

        foreach ($dailyKeyed as $key => $value) {
            $i++;
            $detail = [];
            $detail['minTemperature'] = min($value['minTemperature']);
            $detail['maxTemperature'] = max($value['maxTemperature']);
            $detail['maxWindSpeed'] = max($value['windSpeed']);
            $detail['pressure'] = round(array_sum($value['pressure'])/count($value['pressure']));
            $detail['humidity'] = round(array_sum($value['humidity'])/count($value['humidity']));
            $detail['windDirectionDeg'] = round(array_sum($value['windDirectionDeg'])/count($value['windDirectionDeg']));

            if(is_array($value['precipitation']))
                $detail['precipitation'] = array_sum($value['precipitation']);
            elseif (is_array($value['precipitation_2'])) {
                $detail['precipitation'] = array_sum($value['precipitation_2']);
            }
            $detail['cloudiness'] = round(array_sum($value['cloudiness'])/count($value['cloudiness']));


            $todayKey = date('Y-m-d');
            if(
                $key!==$todayKey
            ){
                if(count($value['symbol_original']) === 24){
                    $first = array_chunk($value['symbol_original'], 12)[0];
                    $second = array_chunk($value['symbol_original'], 12)[0];
                    $firstValues = array_count_values($first);
                    $max = max($firstValues);
                    foreach($firstValues as $k=>$v){
                        if($v==$max){
                            //$detail['symbol_original_d'] = $k;
                            $detail['symbol_d'] = $this->translateIcons($k, true);
                            break;
                        }
                    }
                    $secondValues = array_count_values($second);
                    $max = max($secondValues);
                    foreach($secondValues as $k=>$v){
                        if($v==$max){
                            //$detail['symbol_original_n'] = $k;
                            $detail['symbol_n'] = $this->translateIcons($k, false);
                            break;
                        }
                    }                 
                }
                if(count($value['symbol_original']) !== 24 && count($value['symbol_original_6']) === 4){
                    //$detail['symbol_original_d'] = $value['symbol_original_6'][1];
                    $detail['symbol_d'] = $this->translateIcons($value['symbol_original_6'][1], true);
                    //$detail['symbol_original_n'] = $value['symbol_original_6'][3];
                    $detail['symbol_n'] = $this->translateIcons($value['symbol_original_6'][3], false);                    
                }
            }

            if(is_array($value['symbol_original'])){
                $values = array_count_values($value['symbol_original']);
                //$detail['symbol_original_sort'] = $values;
                $max = max($values);
                foreach($values as $k=>$v){
                    if($v==$max){
                        $detail['symbol_original'] = $k;
                        $detail['symbol'] = $this->translateIcons($k, true);
                        break;
                    }
                }

            }elseif (is_array($value['symbol_original_2'])) {
                $values = array_count_values($value['symbol_original_2']);
                //$detail['symbol_original_sort'] = $values;
                $max = max($values);
                foreach($values as $k=>$v){
                    if($v==$max){
                        $detail['symbol_original'] = $k;
                        $detail['symbol'] = $this->translateIcons($k, true);
                        break;
                    }
                }
            }

            //$detail = $value;
            $detail['date'] = strtotime($key);
            $daily[] = $detail;

        }

        //print_r($daily);

        $out['daily'] = $daily;

        return $out;
    }

    public function translateIcons($id, $sun){
        /**********************
        https://beta.api.met.no/weatherapi/weathericon/1.1?symbol=21&is_night=0&content_type=image%2Fpng

        1 Sun
        2 LightCloud
        3 PartlyCloud
        4 Cloud
        5 LightRainSun
        6 LightRainThunderSun
        7 SleetSun
        8 SnowSun
        9 LightRain
        10 Rain
        11 RainThunder
        12 Sleet
        13 Snow
        14 SnowThunder
        15 Fog
        20 SleetSunThunder
        21 SnowSunThunder
        22 LightRainThunder
        23 SleetThunder
        24 DrizzleThunderSun
        25 RainThunderSun
        26 LightSleetThunderSun
        27 HeavySleetThunderSun
        28 LightSnowThunderSun
        29 HeavySnowThunderSun
        30 DrizzleThunder
        31 LightSleetThunder
        32 HeavySleetThunder
        33 LightSnowThunder
        34 HeavySnowThunder
        40 DrizzleSun
        41 RainSun
        42 LightSleetSun
        43 HeavySleetSun
        44 LightSnowSun
        45 HeavysnowSun
        46 Drizzle
        47 LightSleet
        48 HeavySleet
        49 LightSnow
        50 HeavySnow
        /**********************/

        /**********************
        gewitter.png
        gewitter_mond.png
        gewitter_sonne.png
        mond.png
        nebel.png
        schnee.png
        schneesturm.png
        schnee_mond.png
        schnee_sonne.png
        sonne.png
        wind.png
        wind_mond.png
        wind_sonne.png
        woke_niesel_mond.png
        woke_niesel_sonne.png
        wolke.png
        wolke_mond.png
        wolke_niesel.png
        wolke_regen.png
        wolke_regen_mond.png
        wolke_regen_sonne.png
        wolke_schauer.png
        wolke_schauer_mond.png
        wolke_schauer_sonne.png
        wolke_sonne.png
        /**********************/


				
        $file = 'wolke';

        switch($id){
            case 1 :
                $file = 'sonne.png';
                if(!$sun)$file = 'mond.png';
                break;
            case ($id == 2 || $id == 3) :
                $file = 'wolke_sonne.png';
                if(!$sun)$file = 'wolke_mond.png';
                break;
            case 4 :
                $file = 'wolke.png';
                break;
            case ($id == 5 || $id == 43):
                $file = 'wolke_regen_sonne.png';
                if(!$sun)$file = 'wolke_regen_mond.png';
                break;
            case 6 :
                $file = 'gewitter_sonne.png';
                if(!$sun)$file = 'gewitter_mond.png';
                break;
            case ($id == 7 || $id == 40 || $id == 42):
                $file = 'woke_niesel_sonne.png';
                if(!$sun)$file = 'woke_niesel_mond.png';
                break;
            case 8 :
                $file = 'schnee_sonne.png';
                if(!$sun)$file = 'schnee_mond.png';
                break;
            case ( $id == 46 || $id == 47):
                $file = 'wolke_niesel.png';
                break;
            case ($id == 9 || $id == 10) :
                $file = 'wolke_regen.png';
                break;
            case ($id == 11 || $id == 22 || $id == 23 || $id == 30 || $id == 31 || $id == 32 || $id == 33 || $id == 34):
                $file = 'gewitter.png';
                break;
            case ($id == 12 || $id == 13 || $id == 14) :
                $file = 'schnee.png';
                break;
            case 15 :
                $file = 'nebel.png';
                break;
            case ($id == 20 || $id == 21 || $id == 24 || $id == 25 || $id == 26 || $id == 27 || $id == 28 || $id == 29):
                $file = 'gewitter_sonne.png';
                if(!$sun)$file = 'gewitter_mond.png';
                break;
            case 41 :
                $file = 'wolke_schauer_sonne.png';
                if(!$sun)$file = 'wolke_schauer_mond.png';
                break;
            case ($id == 44 || $id == 45):
                $file = 'schnee_sonne.png';
                if(!$sun)$file = 'schnee_mond.png';
                break;
            case ($id == 48 || $id == 49 || $id == 50):
                $file = 'schnee.png';
                break;
        }

        return $file;
    }
    
    public function testdailyicons($lat, $lng){
        $timezone = "Europe/Berlin"; 
    	 $query = sprintf('http://api.met.no/weatherapi/locationforecast/1.9/?lat=%s;lon=%s',
            $lat,
            $lng
        );

        $result = $this->_getUrlContent($query);

        $content = $result['content'];

        $xml = simplexml_load_string($content);
    			//print_r($xml->product);
    			
    			$product = $xml->product;
    			
    	
    			
    			foreach($product->time as $hour){
                            $from = new DateTime($hour['from']);
                            $from->setTimezone(new DateTimeZone($timezone));
                            $to = new DateTime($hour['to']);
                            $to->setTimezone(new DateTimeZone($timezone));
                            $test=$from;
                                if($test->modify('+ 1 hour') == $to){
                                    $d=$from->format('j');
                                    $testnight= $to->format('H');
                                    $testday= $from->format('H');
                                    $sun_info = date_sun_info(strtotime(date("Y-m-".$d)),$lat,$lng);
                                    $night=0;
                                    if($testnight > date("H",$sun_info['sunset'])){$night = 1;};
                                    if($testday < date("H",$sun_info['sunrise'])){$night = 1;};
    					   			
                                        $symbol = array(
                                                "id" => $hour->location->symbol['number'],
                                                "night"=> $night,
                                                "time"=> $from->format("H"),
                                        );
                                        
                                        $dailyIds[$d][]=$symbol;
                                }
                                    $test=$from;
                                    if($test->modify('+ 5 hours') == $to and $test->format("j")>=date("j",strtotime("+ 3 days")) ){
                                        $d=$from->format('j');
					$testnight= $to->format('H');
					$testday= $to->format('H');
					$sun_info = date_sun_info(strtotime(date("Y-m-".$d)),$lat,$lng);
					$night=0;
					if($testnight > date("H",$sun_info['sunset'])){$night = 1;};
					if($testday < date("H",$sun_info['sunrise'])){$night = 1;};
					$symbol = array(
    					   "id" => $hour->location->symbol['number'],
    					   "night"=> $night,
    					   "time"=> $from->format("H"),
    					   			);
    							$dailyIds[$d][]=$symbol;
									}
    						}
              
    		return $dailyIds;
    }
    
    public function testicons(){
			$icon = array();
			$sun = true;
			$notallowed= array(16,17,18,19,35,36,37,38,39);
			for($i=1; $i < 50; $i++){
				if(!in_array($i,$notallowed)){
					$filename=$this->translateIcons($i,$sun);
					$iconday[$filename][$i]=$i;
					};				
				
			}
			$sun = false;
			for($i=1; $i < 50; $i++){
				if(!in_array($i,$notallowed)){
					$filename=$this->translateIcons($i,$sun);
					$iconnight[$filename][$i]=$i;
				}			
			}
			
			//var_dump($icon);
			echo "<table>";
			foreach($iconday as $translatediconname => $translatediconid){
				echo "<tr>";
				echo "<th>".$translatediconname."</th>";
				foreach($translatediconid as $originalid){
					echo"<th><img src='https://beta.api.met.no/weatherapi/weathericon/1.1?symbol=".$originalid."&is_night=0&content_type=image%2Fpng'</th>";
				}
					echo"</tr>";
				}
			foreach($iconnight as $translatediconname => $translatediconid){
				echo "<tr>";
				echo "<th>".$translatediconname."</th>";
				foreach($translatediconid as $originalid){
					echo"<th><img src='https://beta.api.met.no/weatherapi/weathericon/1.1?symbol=".$originalid."&is_night=1&content_type=image%2Fpng'</th>";
				}
					echo"</tr>";
				}
				echo"</table>";
		}
	
    public function testtranslateicons($id){
			
			// code = sun/cloud/rain/snow/thunder/fog
			$code = '000000';
			//later translated into this 
			$perms = array(
				"sun" => 0,
				"cloud" => 0,
				"rain" => 0,
				"snow" => 0,
				"thunder" => 0,
				"fog" => 0,
			);
			
			switch($id){
				case 1 :
							$code = '100000';
							break;
				case 2 :
							$code = '110000';
							break;
				case 3 :
							$code = '110000';
							break;
				case 4 :
							$code = '010000';
							break;
				case 5 :
							$code = '112000';
							break;
				case 6 :
							$code = '112010';
							break;
				case 7 :
							$code = '111100';
							break;
				case 8 :
							$code = '110200';
							break;
				case 9 :
							$code = '012000';
							break;
				case 10 :
							$code = '013000';
							break;
				case 11 :
							$code = '013010';
							break;
				case 12 :
							$code = '011100';
							break;
				case 13 :
							$code = '010200';
							break;
				case 14 :
							$code = '010210';
							break;
				case 15 :
							$code = '000001';
							break;
				case 20 :
							$code = '111110';
							break;
				case 21 :
							$code = '110210';
							break;
				case 22 :
							$code = '012010';
							break;
				case 23 :
							$code = '011110';
							break;
				case 24 :
							$code = '111010';
							break;
				case 25 :
							$code = '113010';
							break;
				case 26 :
							$code = '111110';
							break;
				case 27 :
							$code = '112110';
							break;
				case 28 :
							$code = '110110';
							break;
				case 29 :
							$code = '110310';
							break;
				case 30 :
							$code = '011000';
							break;
				case 31 :
							$code = '011110';
							break;
				case 32 :
							$code = '012110';
							break;
				case 33 :
							$code = '010110';
							break;
				case 34 :
							$code = '010310';
							break;
				case 40 :
							$code = '111000';
							break;
				case 41 :
							$code = '113000';
							break;
				case 42 :
							$code = '111100';
							break;
				case 43 :
							$code = '112100';
							break;
				case 44 :
							$code = '110100';
							break;
				case 45 :
							$code = '110300';
							break;
				case 46 :
							$code = '011000';
							break;
				case 47 :
							$code = '011100';
							break;
				case 48 :
							$code = '012100';
							break;
				case 49 :
							$code = '010100';
							break;
				case 50 :
							$code = '010300';
							break;			
			}
			
			//perms werte zuweisen
			$perms['sun'] = $code[0];
			$perms['cloud'] = $code[1];
			$perms['rain'] = $code[2];
			$perms['snow'] = $code[3];
			$perms['thunder'] = $code[4];
			$perms['fog'] = $code[5];
			
			return $perms;
		}
		
    public function rulez($perms){
		
		if(isset($perms['sun'])){
				if($perms['sun']== 0){
				$perms['cloud']=1;
				};
			}
		if(isset($perms['moon'])){
				if($perms['moon']== 0){
				$perms['cloud']=1;
				};
			}	
		
			if($perms['rain']>$perms['snow']){
				$perms['snow']=0;
				}else{
				$perms['rain']=0;
				};
			
			return $perms;
		}
		
    public function createFileName($list){
		$namelist=array();
		$thename='';
		
		foreach($list as $day => $time){
			$thename='';
			
			if($time['day']['sun']==1){$thename.='sunny_';};	
			if($time['day']['cloud']==1){$thename.='cloudy_';};
			if($time['day']['rain']==1){$thename.='drizzly_';}elseif($time['day']['rain']==2){$thename.='rainy_';}elseif($time['day']['rain']==3){$thename.='strongrain_';};
			if($time['day']['snow']==1){$thename.='drossy_';}elseif($time['day']['snow']==2){$thename.='snowy_';}elseif($time['day']['snow']==3){$thename.='snowstorm_';};
			if($time['day']['thunder']==1){$thename.='thundery_';};
			if($time['day']['fog']==1){$thename.='foggy_';};
			$thename.='.png';
			$namelist[$day]['day']=$thename;
			
			$thename='';
			
			if($time['night']['moon']==1){$thename.='moon_';};	
			if($time['night']['cloud']==1){$thename.='cloudy_';};
			if($time['night']['rain']==1){$thename.='drizzly_';}elseif($time['day']['rain']==2){$thename.='rainy_';}elseif($time['day']['rain']==3){$thename.='strongrain_';};
			if($time['night']['snow']==1){$thename.='drossy_';}elseif($time['day']['snow']==2){$thename.='snowy_';}elseif($time['day']['snow']==3){$thename.='snowstorm_';};
			if($time['night']['thunder']==1){$thename.='thundery_';};
			if($time['night']['fog']==1){$thename.='foggy_';};
			$thename.='.png';
			$namelist[$day]['night']=$thename;
			}
			
			return $namelist;
		}
		
    public function testLocationIconForecast($lat, $lng){
		
$query = sprintf('http://api.met.no/weatherapi/locationforecast/1.9/?lat=%s;lon=%s',
            $lat,
            $lng
        );

        $result = $this->_getUrlContent($query);

        $content = $result['content'];

        $xml = simplexml_load_string($content);
    			//print_r($xml->product);
    			
    			$product = $xml->product;
					
					$testHourlyKeyd= array();
    			
    			foreach($product->time as $hour){
						$from = new DateTime($hour['from']);
    					$to = new DateTime($hour['to']);
    				$test=$from;				
    					   if($test->modify('+ 1 hour') == $to){
    					   
									$d=$to->format('j');
    			    		$h=$to->format('G');
    			    		$testHourlyKeyd[$d][$h]['symbol'][]=$this->testtranslateicons($hour->location->symbol['number']); 
									}
						$test=$from;
								if($test->modify('+ 5 hours') == $to && $test->format("j")>=date("j",strtotime("+ 3 days"))){
									
									$d=$to->format('j');
    			    		$h=$to->format('G');
    			    		$testHourlyKeyd[$d][$h]['symbol'][]=$this->testtranslateicons($hour->location->symbol['number']); 
									}
    			}	
    			
					
    		  foreach($testHourlyKeyd as $day => $val){
    		  		
    		  		
							$sun_info = date_sun_info(strtotime(date("Y-m-".$day)), $lat, $lng);
							
							$testsunrise=date("H:i:s",$sun_info['sunrise']);
							$testsunset=date("H:i:s",$sun_info['sunset']);
							
							$beginn =new DateTime (date("Y-m-".$day." ".$testsunrise));
							$end = new DateTime (date("Y-m-".$day." ".$testsunset));
							
							
    					for($i = $beginn; $i <= $end; $i->modify('+ 1 hour')){
    						$d=$i->format("j");
    						$h=$i->format("G");
    						if(isset($testHourlyKeyd[$d][$h])){
    							foreach($testHourlyKeyd[$d][$h]['symbol']as $symbolcode){
    								$sumkeysday['sun']['sum']+=$symbolcode['sun'];
    								$sumkeysday['sun']['times']++;
    								$sumkeysday['cloud']['sum']+=$symbolcode['cloud'];
    								$sumkeysday['cloud']['times']++;
    								$sumkeysday['rain']['sum']+=$symbolcode['rain'];
    								$sumkeysday['rain']['times']++;
    								$sumkeysday['snow']['sum']+=$symbolcode['snow'];
    								$sumkeysday['snow']['times']++;
    								$sumkeysday['thunder']['sum']+=$symbolcode['thunder'];
    								$sumkeysday['thunder']['times']++;
    								$sumkeysday['fog']['sum']+=$symbolcode['fog'];
    								$sumkeysday['fog']['times']++;
    							} 
    						}
    					}
    					
    					$beginn =new DateTime (date("Y-m-".$day." ".$testsunset));
							$end = new DateTime (date("Y-m-".($day+1)." ".$testsunrise));
							
    					for($i = $beginn; $i <= $end; $i->modify('+ 1 hour')){
    						$d=$i->format("j");
    						$h=$i->format("G");
    						if(isset($testHourlyKeyd[$d][$h])){
    							foreach($testHourlyKeyd[$d][$h]['symbol']as $symbolcode){
    								$sumkeysnight['moon']['sum']+=$symbolcode['sun'];
    								$sumkeysnight['moon']['times']++;
    								$sumkeysnight['cloud']['sum']+=$symbolcode['cloud'];
    								$sumkeysnight['cloud']['times']++;
    								$sumkeysnight['rain']['sum']+=$symbolcode['rain'];
    								$sumkeysnight['rain']['times']++;
    								$sumkeysnight['snow']['sum']+=$symbolcode['snow'];
    								$sumkeysnight['snow']['times']++;
    								$sumkeysnight['thunder']['sum']+=$symbolcode['thunder'];
    								$sumkeysnight['thunder']['times']++;
    								$sumkeysnight['fog']['sum']+=$symbolcode['fog'];
    								$sumkeysnight['fog']['times']++;
    							} 
    						}
    					}
    				
    				
    				$thefinal[$day]['day']=array(
    				"sun" => $sumkeysday['sun']['times'] === 0 ? 0 : round ( $sumkeysday['sun']['sum'] / $sumkeysday['sun']['times']),
						"cloud" => $sumkeysday['cloud']['times'] === 0 ? 0 : round ( $sumkeysday['cloud']['sum'] / $sumkeysday['cloud']['times']),
						"rain" => $sumkeysday['rain']['times'] === 0 ? 0 : round ( $sumkeysday['rain']['sum'] / $sumkeysday['rain']['times']),
						"snow" => $sumkeysday['snow']['times'] === 0 ? 0 : round ( $sumkeysday['snow']['sum'] / $sumkeysday['snow']['times']),
						"thunder" => $sumkeysday['thunder']['times'] === 0 ? 0 : round ( $sumkeysday['thunder']['sum'] / $sumkeysday['thunder']['times']),
						"fog" => $sumkeysday['fog']['times'] === 0 ? 0 : round ( $sumkeysday['fog']['sum'] / $sumkeysday['fog']['times']),
    				);
    			
    				$thefinal[$day]['night']=array(
    				"moon" => $sumkeysnight['moon']['times'] === 0 ? 0 : round( $sumkeysnight['moon']['sum'] / $sumkeysnight['moon']['times']),
						"cloud" =>$sumkeysnight['cloud']['times'] === 0 ? 0 : round( $sumkeysnight['cloud']['sum'] / $sumkeysnight['cloud']['times']),
						"rain" => $sumkeysnight['rain']['times'] === 0 ? 0 :  round( $sumkeysnight['rain']['sum'] / $sumkeysnight['rain']['times']),
						"snow" => $sumkeysnight['snow']['times'] === 0 ? 0 :  round( $sumkeysnight['snow']['sum'] / $sumkeysnight['snow']['times']),
						"thunder" => $sumkeysnight['thunder']['times'] === 0 ? 0 :  round( $sumkeysnight['thunder']['sum'] / $sumkeysnight['thunder']['times']),
						"fog" => $sumkeysnight['fog']['times'] === 0 ? 0 :  round( $sumkeysnight['fog']['sum'] / $sumkeysnight['fog']['times']),
    				);
    			
    				
    				$thefinal[$day]['day']= $this->rulez($thefinal[$day]['day']);
    				$thefinal[$day]['night']= $this->rulez($thefinal[$day]['night']);
    				
    				
    		  } 			
    			//var_dump($thefinal);
    			$thefiles=$this->createFileName($thefinal);
    			//var_dump($thefiles);
    			return $thefiles;	
		}
		
		
}
?>
