<?PHP

class weathersymb {
    
    private function _getUrlContent($query){

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $query);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt ($ch, CURLOPT_HEADER, 0);
        curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_TIMEOUT, 50);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 6);
        curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt ($ch, CURLOPT_HTTPHEADER,array('User-agent: Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.16) Gecko/20080702 Firefox/2.0.0.16', 'Accept-language: en-us,en;q=0.7,bn;q=0.3', 'Accept-charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7'));
        $content = curl_exec($ch);
        $httpStatus = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $lastUrl = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);


        return array(
            'content' => $content,
            'httpStatus' => $httpStatus,
            'lastUrl' => $lastUrl
        );

    }
    
    public function translateicons($id , $night){
        
        // code = night/clouds/thunder/rain/snow/sleet/fog
        /*
         * night: 1=yes(all "sun" is "moon" ) / 0=no
         * clouds: 0=sun / 1=sun_cloudy / 2=cloudy
         * thunder: 1=yes / 0=no
         * rain: 0=none / 1=drizzly / 2=rain / 3=strongrain
         * snow: 0=none / 1=lightsnow / 2=snow / 3=strongsnow 
         * sleet: 0=none / 1=drossy / 2=sleet / 3=strongsleet
         * fog: 1=yes / 0=no
         */
        
        $code = '000000';
	//later translated into this
        $perms = [
            "night" => 0,
            "cloud" => 0,
            "thunder" => 0,
            "rain" => 0,
            "snow" => 0,
            "sleet" => 0,
            "fog" => 0,
            ];
        
        //start translation 
        switch($id){
            case 1 :
                if($night){$code = '1000000';}else{$code = '0000000';} 
		break;
            case 2 :
                if($night){$code = '1100000';}else{$code = '0100000';} 
		break;
            case ($id == 3 || $id == 4) :
                if($night){$code = '1200000';}else{$code = '0200000';} 
		break;
            case 5 :
                if($night){$code = '1102000';}else{$code = '0102000';} 
		break;
            case 6 :
                if($night){$code = '1112000';}else{$code = '0112000';} 
		break;
            case 7 :
                if($night){$code = '1100020';}else{$code = '0100020';} 
		break;
            case 8 :
                if($night){$code = '1100200';}else{$code = '0100200';} 
		break;
            case 9 :
                if($night){$code = '1202000';}else{$code = '0202000';} 
		break;
            case 10 :
                if($night){$code = '1203000';}else{$code = '0203000';} 
		break;
            case 11 :
                if($night){$code = '1212000';}else{$code = '0212000';} 
		break;
            case 12 :
                if($night){$code = '1200020';}else{$code = '0200020';} 
		break;
            case 13 :
                if($night){$code = '1200200';}else{$code = '0200200';} 
		break;
            case 14 :
                if($night){$code = '1210200';}else{$code = '1210200';} 
		break;
            case 15 :
                if($night){$code = '1000001';}else{$code = '0000001';} 
		break;
            case 20 :
                if($night){$code = '1110020';}else{$code = '0110020';} 
		break;
            case 21 :
                if($night){$code = '1110200';}else{$code = '0110200';} 
		break;
            case 22 :
                if($night){$code = '1212000';}else{$code = '0212000';} 
		break;
            case 23 :
                if($night){$code = '1210020';}else{$code = '0210020';} 
		break;
            case 24 :
                if($night){$code = '1111000';}else{$code = '0111000';} 
		break;
            case 25 :
                if($night){$code = '1113000';}else{$code = '1113000';} 
		break;
            case 26 :
                if($night){$code = '1110010';}else{$code = '0110010';} 
		break;
            case 27 :
                if($night){$code = '1110030';}else{$code = '1110030';} 
		break;
            case 28 :
                if($night){$code = '1110100';}else{$code = '0110100';} 
		break;
            case 29 :
                if($night){$code = '1110300';}else{$code = '1110300';} 
		break;
            case 30 :
                if($night){$code = '1211000';}else{$code = '1211000';} 
		break;
            case 31 :
                if($night){$code = '1210010';}else{$code = '1210010';} 
		break;
            case 32 :
                if($night){$code = '1210030';}else{$code = '0210030';} 
		break;
            case 33 :
                if($night){$code = '1210100';}else{$code = '0210100';} 
		break;
            case 34 :
                if($night){$code = '1110300';}else{$code = '1110300';} 
		break;
            case 40 :
                if($night){$code = '1101000';}else{$code = '0101000';} 
		break;
            case 41 :
                if($night){$code = '1103000';}else{$code = '1103000';} 
		break;
            case 42 :
                if($night){$code = '1100010';}else{$code = '0100010';} 
		break;
            case 43 :
                if($night){$code = '1100020';}else{$code = '0100020';} 
		break;
            case 44 :
                if($night){$code = '1100100';}else{$code = '0100100';} 
		break;
            case 45 :
                if($night){$code = '1100300';}else{$code = '1100300';} 
		break;
            case 46 :
                if($night){$code = '1201000';}else{$code = '0201000';} 
		break;
            case 47 :
                if($night){$code = '1200010';}else{$code = '0200010';} 
		break;
            case 48 :
                if($night){$code = '1200020';}else{$code = '0200020';} 
		break;
            case 49 :
                if($night){$code = '1200100';}else{$code = '0200100';} 
		break;
            case 50 :
                if($night){$code = '1200300';}else{$code = '0200300';} 
		break;
	}
        
        $perms['night'] =(int) $code[0];
	$perms['cloud'] =(int) $code[1];
        $perms['thunder'] =(int) $code[2];
	$perms['rain'] =(int) $code[3];
	$perms['snow'] =(int) $code[4];
	$perms['sleet'] =(int) $code[5];
	$perms['fog'] =(int) $code[6];
        $perms['id']= $id;
        
        return $perms;
    }
    
    public function createFilename($list, $night){
        $name ='';
        
        
        if($list['cloud'] == 0){$name.= $night == false ? "sunny_" : "moon_";}
        elseif($list['cloud'] == 1 ){$name.= $night == false ? "sunny_cloudy_" : "moon_cloudy_";}
        elseif($list['cloud'] == 2 ){$name.= "cloudy_";}
        if($list['thunder'] == 1){$name.='thundery_';}
        if($list['rain']==1){$name.='drizzle_';}elseif($list['rain']==2){$name.='rain_';}elseif($list['rain']==3){$name.='strongrain_';}
        if($list['snow']==1){$name.='lightsnow_';}elseif($list['snow']==2){$name.='snow_';}elseif($list['snow']==3){$name.='snowstorm_';}
        if($list['sleet']==1){$name.='drossy_';}elseif($list['sleet']==2){$name.='sleet_';}elseif($list['sleet']==3){$name.='strongsleet_';}
        if($list['fog']==1){$name.="foggy_";}
        $name.=".png";
        
        return $name;
    }
    
    public function testLocationIconForecast($lat, $lng){

        $timezone = "Europe/Berlin"; 	
        
    $query = sprintf('http://api.met.no/weatherapi/locationforecast/1.9/?lat=%s;lon=%s',
            $lat,
            $lng
        );

        $result = $this->_getUrlContent($query);

        $content = $result['content'];

        $xml = simplexml_load_string($content);
        //print_r($xml->product);
    			
        $product = $xml->product;
       
        $symdat= [];
        $sumdaytemp=[];
        $sumnighttemp=[];
        foreach($product->time as $time){
            
            $from = new DateTime($time['from']);
            $from->setTimezone(new DateTimeZone($timezone));
            $to = new DateTime($time['to']);
            $to->setTimezone(new DateTimeZone($timezone));
            
            
            $interval = $from->diff($to);
            $now = new DateTime();
            $now->setTimezone(new DateTimeZone($timezone));
            $nowthen = $now->diff($to);
            
            if(($interval->h == 1) || ($nowthen->d >=3 && $interval->h >=5)){
                
                $d=$to->format("j");
                $h=$to->format("G");
                
                $sunrise = date_sunrise(strtotime($time['to']), SUNFUNCS_RET_TIMESTAMP, $lat , $lng);
                $sunset = date_sunset(strtotime($time['to']), SUNFUNCS_RET_TIMESTAMP, $lat , $lng);                
                
                
                if(($to->getTimestamp() >= $sunset) || ($from->getTimestamp() <= $sunrise) && $nowthen->d <3){ 
                    $night = true;
                    
                }elseif($nowthen->d >=3 && $h>= 20 || $h <= 2){
                    $night = true;
                }elseif($nowthen->d >=3 && $h>= 8 ) {
                    $night = false;
                }else{
                    $night=false;
                }

                $symcode= $this->translateicons($time->location->symbol['number'], $night);          

                $symdat[$d]['hourly'][$h]= $symcode;
                }
            }
            
                foreach( $symdat as $date => $day){
                    //var_dump($symdat);
                $sunrise = date("H:i:s",date_sunrise(strtotime(date("Y-m-".$date)), SUNFUNCS_RET_TIMESTAMP, $lat , $lng));
                $sunset = date("H:i:s",date_sunset(strtotime(date("Y-m-".$date)), SUNFUNCS_RET_TIMESTAMP, $lat , $lng));
                $sunrisetomorrow = date("H:i:s",date_sunrise(strtotime(date("Y-m-".($date+1))), SUNFUNCS_RET_TIMESTAMP, $lat, $lng));    
                
                $beginn  = new DateTime( date("Y-m-".$date." ".$sunrise));   
                $end =  new DateTime(date ("Y-m-".($date+1)." ".$sunrisetomorrow));
                
                for($i=$beginn; $i <= $end; $i->modify('+ 1 hour')){
                    $d=$i->format("j");
                    $h=$i->format("G");
                 
                if(isset($symdat[$d]['hourly'][$h])){    
                 if($symdat[$d]['hourly'][$h]['night']!=1){
                    
                    if($symdat[$d]['hourly'][$h]['cloud'] >= 0){
                    isset($sumdaytemp['cloud']['code']) ?  $sumdaytemp['cloud']['code'] += $symdat[$d]['hourly'][$h]['cloud'] : $sumdaytemp['cloud']['code'] = $symdat[$d]['hourly'][$h]['cloud'];
                    isset($sumdaytemp['cloud']['times']) ? $sumdaytemp['cloud']['times']++ : $sumdaytemp['cloud']['times'] = 1;
                    }
                    
                    if($symdat[$d]['hourly'][$h]['thunder'] > 0){
                    isset($sumdaytemp['thunder']['code']) ? $sumdaytemp['thunder']['code'] += $symdat[$d]['hourly'][$h]['thunder'] : $sumdaytemp['thunder']['code'] = $symdat[$d]['hourly'][$h]['thunder'];
                    isset($sumdaytemp['thunder']['times']) ? $sumdaytemp['thunder']['times']++ : $sumdaytemp['thunder']['times'] = 1;
                    }
                    
                    if($symdat[$d]['hourly'][$h]['rain'] > 0){
                    isset($sumdaytemp['rain']['code']) ?  $sumdaytemp['rain']['code'] += $symdat[$d]['hourly'][$h]['rain'] : $sumdaytemp['rain']['code'] = $symdat[$d]['hourly'][$h]['rain'];
                    isset($sumdaytemp['rain']['times']) ? $sumdaytemp['rain']['times']++ : $sumdaytemp['rain']['times'] = 1;
                    }
                    
                    if($symdat[$d]['hourly'][$h]['snow'] > 0){
                    isset($sumdaytemp['snow']['code']) ?  $sumdaytemp['snow']['code'] += $symdat[$d]['hourly'][$h]['snow'] : $sumdaytemp['snow']['code'] = $symdat[$d]['hourly'][$h]['snow'];
                    isset($sumdaytemp['snow']['times']) ? $sumdaytemp['snow']['times']++ : $sumdaytemp['snow']['times'] = 1;
                    }
                    
                    if($symdat[$d]['hourly'][$h]['sleet'] > 0){
                    isset($sumdaytemp['sleet']['code']) ?  $sumdaytemp['sleet']['code'] += $symdat[$d]['hourly'][$h]['sleet'] : $sumdaytemp['sleet']['code'] = $symdat[$d]['hourly'][$h]['sleet'];
                    isset($sumdaytemp['sleet']['times']) ? $sumdaytemp['sleet']['times']++ : $sumdaytemp['sleet']['times'] = 1;
                    }
                    
                    if($symdat[$d]['hourly'][$h]['fog'] > 0){
                    isset($sumdaytemp['fog']['code']) ?  $sumdaytemp['fog']['code'] += $symdat[$d]['hourly'][$h]['fog'] : $sumdaytemp['fog']['code'] = $symdat[$d]['hourly'][$h]['fog'];
                    isset($sumdaytemp['fog']['times']) ? $sumdaytemp['fog']['times']++ : $sumdaytemp['fog']['times'] = 1;
                    }
                }else{
                    
                    if($symdat[$d]['hourly'][$h]['cloud'] >= 0){
                    isset($sumnighttemp['cloud']['code']) ? $sumnighttemp['cloud']['code'] += $symdat[$d]['hourly'][$h]['cloud'] : $sumnighttemp['cloud']['code'] = $symdat[$d]['hourly'][$h]['cloud'];
                    isset($sumnighttemp['cloud']['times']) ? $sumnighttemp['cloud']['times']++ : $sumnighttemp['cloud']['times'] = 1; 
                    }
                    
                    if($symdat[$d]['hourly'][$h]['thunder'] > 0){
                    isset($sumnighttemp['thunder']['code']) ? $sumnighttemp['thunder']['code'] += $symdat[$d]['hourly'][$h]['thunder'] : $sumnighttemp['thunder']['code'] = $symdat[$d]['hourly'][$h]['thunder'];
                    isset($sumnighttemp['thunder']['times']) ? $sumnighttemp['thunder']['times']++ : $sumnighttemp['thunder']['times'] = 1;
                    }
                    
                    if($symdat[$d]['hourly'][$h]['rain'] > 0){
                    isset($sumnighttemp['rain']['code']) ? $sumnighttemp['rain']['code'] += $symdat[$d]['hourly'][$h]['rain'] : $sumnighttemp['rain']['code'] = $symdat[$d]['hourly'][$h]['rain'];
                    isset($sumnighttemp['rain']['times']) ? $sumnighttemp['rain']['times']++ : $sumnighttemp['rain']['times'] = 1;
                    }
                    
                    if($symdat[$d]['hourly'][$h]['snow'] > 0){
                    isset($sumnighttemp['snow']['code']) ? $sumnighttemp['snow']['code'] += $symdat[$d]['hourly'][$h]['snow'] : $sumnighttemp['snow']['code'] = $symdat[$d]['hourly'][$h]['snow'];
                    isset($sumnighttemp['snow']['times']) ? $sumnighttemp['snow']['times']++ : $sumnighttemp['snow']['times'] = 1;
                    }
                    
                    if($symdat[$d]['hourly'][$h]['sleet'] > 0){
                    isset($sumnighttemp['sleet']['code']) ? $sumnighttemp['sleet']['code'] += $symdat[$d]['hourly'][$h]['sleet'] : $sumnighttemp['sleet']['code'] = $symdat[$d]['hourly'][$h]['sleet'];
                    isset($sumnighttemp['sleet']['times']) ? $sumnighttemp['sleet']['times']++ : $sumnighttemp['sleet']['times'] = 1;
                    }
                    
                    if($symdat[$d]['hourly'][$h]['fog'] > 0){
                    isset($sumnighttemp['fog']['code']) ? $sumnighttemp['fog']['code'] += $symdat[$d]['hourly'][$h]['fog'] : $sumnighttemp['fog']['code'] = $symdat[$d]['hourly'][$h]['fog'];
                    isset($sumnighttemp['fog']['times']) ? $sumnighttemp['fog']['times']++ : $sumnighttemp['fog']['times'] = 1;
                    }
                }
                }    
                }
                  
                    $symdat[$date]['daysymb']['cloud']=isset($sumdaytemp['cloud']['times']) ?  round($sumdaytemp['cloud']['code'] / $sumdaytemp['cloud']['times']) : 0 ;
                    $symdat[$date]['daysymb']['thunder']=isset($sumdaytemp['thunder']['times']) ?  round($sumdaytemp['thunder']['code'] / $sumdaytemp['thunder']['times']) : 0 ;
                    $symdat[$date]['daysymb']['rain']=isset($sumdaytemp['rain']['times']) ?  round($sumdaytemp['rain']['code'] / $sumdaytemp['rain']['times']) : 0 ;
                    $symdat[$date]['daysymb']['snow']=isset($sumdaytemp['snow']['times']) ?  round($sumdaytemp['snow']['code'] / $sumdaytemp['snow']['times']) : 0 ;
                    $symdat[$date]['daysymb']['sleet']=isset($sumdaytemp['sleet']['times']) ?  round($sumdaytemp['sleet']['code'] / $sumdaytemp['sleet']['times']) : 0 ;        
                    $symdat[$date]['daysymb']['fog']=isset($sumdaytemp['fog']['times']) ?  round($sumdaytemp['fog']['code'] / $sumdaytemp['fog']['times']) : 0 ;
                    $symdat[$date]['daysymb']['name']= $this->createFilename($symdat[$date]['daysymb'], false); 
                
                    $symdat[$date]['nightsymb']['cloud']=isset($sumnighttemp['cloud']['times']) ?  round($sumnighttemp['cloud']['code'] / $sumnighttemp['cloud']['times']) : 0 ;
                    $symdat[$date]['nightsymb']['thunder']=isset($sumnighttemp['thunder']['times']) ?  round($sumnighttemp['thunder']['code'] / $sumnighttemp['thunder']['times']) : 0 ;
                    $symdat[$date]['nightsymb']['rain']=isset($sumnighttemp['rain']['times']) ?  round($sumnighttemp['rain']['code'] / $sumnighttemp['rain']['times']) : 0 ;
                    $symdat[$date]['nightsymb']['snow']=isset($sumnighttemp['snow']['times']) ?  round($sumnighttemp['snow']['code'] / $sumnighttemp['snow']['times']) : 0 ;
                    $symdat[$date]['nightsymb']['sleet']=isset($sumnighttemp['sleet']['times']) ?  round($sumnighttemp['sleet']['code'] / $sumnighttemp['sleet']['times']) : 0 ;        
                    $symdat[$date]['nightsymb']['fog']=isset($sumnighttemp['fog']['times']) ?  round($sumnighttemp['fog']['code'] / $sumnighttemp['fog']['times']) : 0 ;
                    $symdat[$date]['nightsymb']['name']= $this->createFilename($symdat[$date]['nightsymb'], true); 
                    
                    unset ($sumdaytemp);
                    unset ($sumnighttemp);
                }
                
           //var_dump($symdat);
    return $symdat;    
    }  
}






